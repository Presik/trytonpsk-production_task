# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition
from trytond.transaction import Transaction


class PayrollPayoffProduction(Wizard):
    'Payroll Recompute'
    __name__ = 'staff.payroll.payoff_production'
    start_state = 'do_payoff'
    do_payoff = StateTransition()

    def transition_do_payoff(self):
        pool = Pool()
        Payroll = pool.get('staff.payroll')
        Line = pool.get('staff.payroll.line')
        TaskLine = pool.get('production.task.line')
        id_ = Transaction().context['active_id']
        config = pool.get('production.configuration')(1)
        payroll = Payroll(id_)

        tasks_lines = TaskLine.search([
            ('employee', '=', payroll.employee.id),
            ('task_date', '>=', payroll.start),
            ('task_date', '<=', payroll.end),
            ('state', '=', 'done'),
        ])

        if not config.task_wage_type:
            return 'end'

        total_amount = Decimal(0)

        for tline in tasks_lines:
            total_amount += tline.cost_amount

        Line.create([{
            'sequence': 1,
            'payroll': payroll.id,
            'wage_type': config.task_wage_type.id,
            'description': config.task_wage_type.name,
            'quantity': 1,
            'uom': config.task_wage_type.uom.id,
            'unit_value': total_amount,
            'receipt': True,
        }])
        return 'end'
