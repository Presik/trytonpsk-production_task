# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'production.configuration'
    # task_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
    #     'Task Sequence', required=True, domain=[
    #             ('code', '=', 'production')])
    #     )
    # task_wage_type = fields.Many2One('staff.wage_type', 'Task Wage Type',
    #         required=True)
